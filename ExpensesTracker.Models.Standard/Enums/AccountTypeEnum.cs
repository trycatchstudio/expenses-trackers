﻿namespace ExpensesTracker.Models.Standard.Enums
{
    public enum AccountTypeEnum
    {
        Unknown = 0,
        Cash = 1,
        BankAccount = 2,
        Credit = 3
    }
}
