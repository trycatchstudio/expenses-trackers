﻿namespace ExpensesTracker.Models.Standard.Models
{
    public class FlowPurposeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
