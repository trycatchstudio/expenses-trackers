﻿namespace ExpensesTracker.Models.Standard.Models
{
    public class FlowTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
