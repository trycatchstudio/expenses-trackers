﻿namespace ExpensesTracker.Models.Standard.Models
{
    public class AccountTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
