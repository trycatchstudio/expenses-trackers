﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;
using ExpensesTracker.BusinessLogic.Factories;
using ExpensesTracker.Models;

namespace ExpensesTracker.BusinessLogic.FlowPurposesOperations
{
    public class OGetFlowPurposes : BaseMoneyFlowOperation<int?, IEnumerable<FlowPurposeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowPurposes.ToList().Where(l => l.FlowPurposeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowPurposes.ToList().Select(ModelFactory.Create));
        }
    }
}
