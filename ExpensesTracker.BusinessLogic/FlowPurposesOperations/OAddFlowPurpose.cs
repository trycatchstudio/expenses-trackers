﻿using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.FlowPurposesOperations
{
    public class OAddFlowPurpose : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New flow purpose not provided");
            }

            var fp = _context.FlowPurposes.Create();

            fp.PurposeName = Parameters;

            // check if unique
            _context.FlowPurposes.Add(fp);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
