﻿using System.Linq;
using CommonStuff.Operation;
using EntityFramework.Extensions;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.FlowPurposesOperations
{
    public class ODeleteFlowPurposee : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            _context.FlowPurposes.Where(l => l.FlowPurposeId == Parameters).Delete();
            _context.SaveChanges();
            return Issue.Success;
        }
    }
}
