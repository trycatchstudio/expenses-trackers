﻿using System.Linq;
using CommonStuff.Operation;
using EntityFramework.Extensions;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.MoneyFlowsOperations
{
    public class ODeleteMoneyFlow : BaseMoneyFlowOperation<int, object>
    {
        protected override Issue ExecuteDerived()
        {
            // could be done without extension methods.
            // 1. retrieve object from DB
            // 2. call .Remove()
            _context.MoneyFlows.Where(l => l.MoneyFlowId == Parameters).Delete();
            _context.SaveChanges();
            return Issue.Success;
        }
    }
}
