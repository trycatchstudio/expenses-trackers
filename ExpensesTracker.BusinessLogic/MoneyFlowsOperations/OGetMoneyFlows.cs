﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;
using ExpensesTracker.BusinessLogic.Factories;
using ExpensesTracker.Models;

namespace ExpensesTracker.BusinessLogic.MoneyFlowsOperations
{
    public class OGetMoneyFlows : BaseMoneyFlowOperation<int?, IEnumerable<MoneyFlowModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.MoneyFlows
                                         .Where(l => l.MoneyFlowId == Parameters.Value)
                                         .Include(c => c.FlowAccountType)
                                         .Include(c => c.FlowPurpose)
                                         .Include(c => c.FlowType)
                                         .Include(c => c.FlowUser)
                                         .ToList().Select(ModelFactory.Create));
            }

            return new Issue(_context.MoneyFlows
                                     .Include(c => c.FlowAccountType)
                                     .Include(c => c.FlowPurpose)
                                     .Include(c => c.FlowType)
                                     .Include(c => c.FlowUser)
                                     .ToList().Select(ModelFactory.Create));
        }
    }
}
