﻿using System.Linq;
using ExpensesTracker.Data;
using ExpensesTracker.Data.Models;
using ExpensesTracker.Models;

namespace ExpensesTracker.BusinessLogic.Factories
{
    public static class DataFactory
    {
        public static MoneyFlow Create(FlowContext context, MoneyFlowModel model)
        {
            // todo:
            // try to create generic method to fetch accType / purpose / type from DB
            if (model == null)
            {
                return null;
            }

            var mf = new MoneyFlow()
            {
                Amount = model.Amount,
                FlowAccountType = GetFlowAccountType(context, model.FlowAccountType),
                FlowPurpose = GetFlowPurpose(context, model.FlowPurpose),
                FlowType = GetFlowType(context, model.FlowType),
                FlowUser = GetFlowUser(context, model.FlowUser)
            };

            return mf;
        }

        public static FlowUser Create(UserModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new FlowUser()
            {
                FlowUserId = model.Id,
                UserName = model.Name
            };
        }

        public static FlowAccountType GetFlowAccountType(FlowContext context, string name)
        {
            var fa = context.FlowAccountType.ToList().FirstOrDefault(l => l.AccountTypeName == name);
            return fa;
        }

        public static FlowPurpose GetFlowPurpose(FlowContext context, string name)
        {
            var fa = context.FlowPurposes.ToList().FirstOrDefault(l => l.PurposeName == name);
            return fa;
        }

        public static FlowType GetFlowType(FlowContext context, string name)
        {
            var fa = context.FlowTypes.ToList().FirstOrDefault(l => l.TypeName == name);
            return fa;
        }

        public static FlowUser GetFlowUser(FlowContext context, UserModel model)
        {
            var fa = context.FlowUsers.ToList().FirstOrDefault(l => l.FlowUserId == model.Id);
            return fa;
        }
    }
}
