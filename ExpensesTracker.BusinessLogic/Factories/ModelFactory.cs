﻿using ExpensesTracker.Data.Models;
using ExpensesTracker.Models;

namespace ExpensesTracker.BusinessLogic.Factories
{
    public static class ModelFactory
    {
        public static MoneyFlowModel Create(MoneyFlow mf)
        {
            if (mf == null)
            {
                return null;
            }

           return new MoneyFlowModel()
            {
                Amount = mf.Amount,
                FlowAccountType = mf.FlowAccountType.AccountTypeName,
                FlowPurpose = mf.FlowPurpose.PurposeName,
                FlowType = mf.FlowType.TypeName,
                FlowUser = Create(mf.FlowUser),
                MoneyFlowId = mf.MoneyFlowId
            };
        }

        public static UserModel Create(FlowUser fu)
        {
            if (fu == null)
            {
                return null;
            }

            return new UserModel(fu.FlowUserId)
            {
                Name = fu.UserName
            };
        }

        public static AccountTypeModel Create(FlowAccountType fat)
        {
            if (fat == null)
            {
                return null;
            }

            return new AccountTypeModel()
            {
                Id = fat.FlowAccountTypeId,
                Name = fat.AccountTypeName
            };
        }

        public static FlowPurposeModel Create(FlowPurpose fp)
        {
            if (fp == null)
            {
                return null;
            }

            return new FlowPurposeModel()
            {
                Id = fp.FlowPurposeId,
                Name = fp.PurposeName
            };
        }

        public static FlowTypeModel Create(FlowType ft)
        {
            if (ft == null)
            {
                return null;
            }

            return new FlowTypeModel()
            {
                Id = ft.FlowTypeId,
                Name = ft.TypeName
            };
        }
    }
}
