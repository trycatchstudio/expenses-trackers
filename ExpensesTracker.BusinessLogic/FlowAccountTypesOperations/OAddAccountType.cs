﻿using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.FlowAccountTypesOperations
{
    public class OAddAccountType : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New account type not provided");
            }

            var ft = _context.FlowAccountType.Create();

            ft.AccountTypeName = Parameters;

            // TODO: check if unique
            _context.FlowAccountType.Add(ft);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
