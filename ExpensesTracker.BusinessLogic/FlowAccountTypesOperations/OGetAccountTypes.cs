﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;
using ExpensesTracker.BusinessLogic.Factories;
using ExpensesTracker.Models;

namespace ExpensesTracker.BusinessLogic.FlowAccountTypesOperations
{
    public class OGetAccountTypes : BaseMoneyFlowOperation<int?, IEnumerable<AccountTypeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowAccountType.ToList().Where(l => l.FlowAccountTypeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowAccountType.ToList().Select(ModelFactory.Create));
        }
    }
}
