﻿using System.Linq;
using CommonStuff.Operation;
using EntityFramework.Extensions;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.FlowAccountTypesOperations
{
    public class ODeleteAccountType : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            _context.FlowAccountType.Where(l => l.FlowAccountTypeId == Parameters).Delete();
            _context.SaveChanges();
            return Issue.Success;
        }
    }
}
