﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;
using ExpensesTracker.BusinessLogic.Factories;
using ExpensesTracker.Models;

namespace ExpensesTracker.BusinessLogic.FlowTypesOperations
{
    public class OGetFlowTypes : BaseMoneyFlowOperation<int?, IEnumerable<FlowTypeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowTypes.ToList().Where(l => l.FlowTypeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowTypes.ToList().Select(ModelFactory.Create));
        }
    }
}
