﻿using System.Linq;
using CommonStuff.Operation;
using EntityFramework.Extensions;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.FlowTypesOperations
{
    public class ODeleteFlowType : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            _context.FlowTypes.Where(l => l.FlowTypeId == Parameters).Delete();
            _context.SaveChanges();
            return Issue.Success;
        }
    }
}
