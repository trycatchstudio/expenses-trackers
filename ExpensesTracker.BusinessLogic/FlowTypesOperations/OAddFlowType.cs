﻿using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.FlowTypesOperations
{
    public class OAddFlowType : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New Flow type not provided");
            }

            var ft = _context.FlowTypes.Create();

            ft.TypeName = Parameters;

            // TODO: check if unique
            _context.FlowTypes.Add(ft);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
