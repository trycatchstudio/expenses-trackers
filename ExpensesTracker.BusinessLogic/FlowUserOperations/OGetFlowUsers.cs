﻿using System.Linq;
using CommonStuff.Operation;
using ExpensesTracker.BusinessLogic.BaseOperation;
using ExpensesTracker.BusinessLogic.Factories;
using ExpensesTracker.Models;

namespace ExpensesTracker.BusinessLogic.FlowUserOperations
{
    public class OGetFlowUsers : BaseMoneyFlowOperation<int?, UserModel>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowUsers.ToList().Where(l => l.FlowUserId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowUsers.ToList().Select(ModelFactory.Create));
        }
    }
}
