﻿using System.Linq;
using CommonStuff.Operation;
using EntityFramework.Extensions;
using ExpensesTracker.BusinessLogic.BaseOperation;

namespace ExpensesTracker.BusinessLogic.FlowUserOperations
{
    public class ODeleteFlowUser : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            _context.FlowUsers.Where(l => l.FlowUserId == Parameters).Delete();
            _context.SaveChanges();
            return Issue.Success;
        }
    }
}
