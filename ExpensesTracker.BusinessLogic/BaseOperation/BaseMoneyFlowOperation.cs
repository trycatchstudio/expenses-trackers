﻿using System;
using CommonStuff.Operation;
using ExpensesTracker.Data;

namespace ExpensesTracker.BusinessLogic.BaseOperation
{
    public abstract class BaseMoneyFlowOperation<TParameter, TResult> : BasicOperation<TParameter, TResult> where TResult : class
    {
        internal FlowContext _context;

        protected override Issue BeforeExecute()
        {
            try
            {
                _context = new FlowContext();
                return Issue.Success;
            }
            catch (Exception ex)
            {
                return new Issue(ex);
            }
        }

        protected override void AfterExecute()
        {
            try
            {
                // should dispose _context here. Maybe there is some way to use Using block in base class
                _context.Dispose();
            }
            catch (Exception ex)
            {
                // error or shit...
            }
        }
    }
}
