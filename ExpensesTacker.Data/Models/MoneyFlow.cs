﻿namespace ExpensesTracker.Data.Models
{
    /// <summary>
    /// Actual money flow record. Place where magic happens
    /// </summary>
    public class MoneyFlow
    {
        public int MoneyFlowId { get; set; }
        public decimal Amount { get; set; }

        public virtual FlowAccountType FlowAccountType { get; set; }
        public virtual FlowType FlowType { get; set; }
        public virtual FlowPurpose FlowPurpose { get; set; }
        public virtual FlowUser FlowUser { get; set; }
    }
}
