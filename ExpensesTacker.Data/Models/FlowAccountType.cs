﻿using System.Collections.Generic;

namespace ExpensesTracker.Data.Models
{
    /// <summary>
    /// e.g. cash, bank acc, etc.
    /// </summary>
    public class FlowAccountType
    {
        public int FlowAccountTypeId { get; set; }
        public string AccountTypeName { get; set; }

        public virtual List<MoneyFlow> MoneyFlows { get; set; }
    }
}
