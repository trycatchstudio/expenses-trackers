﻿using System.Collections.Generic;

namespace ExpensesTracker.Data.Models
{
    public class FlowUser
    {
        public int FlowUserId { get; set; }
        public string UserName { get; set; }

        public virtual List<MoneyFlow> MoneyFlows { get; set; }
    }
}
