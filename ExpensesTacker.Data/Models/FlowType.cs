﻿using System.Collections.Generic;

namespace ExpensesTracker.Data.Models
{
    /// <summary>
    /// e.g. Income, outcome.
    /// This could have been done having two tables: incomes and outcomes, but we chose this kind of architecture to be able to expand it in the future
    /// </summary>
    public class FlowType
    {
        public int FlowTypeId { get; set; }
        public string TypeName { get; set; }

        public virtual List<MoneyFlow> MoneyFlows { get; set; }
    }
}
