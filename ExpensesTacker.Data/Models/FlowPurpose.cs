﻿using System.Collections.Generic;

namespace ExpensesTracker.Data.Models
{
    /// <summary>
    /// e.g. salary, expenses for food, bakn credit, etc.
    /// </summary>
    public class FlowPurpose
    {
        public int FlowPurposeId { get; set; }
        public string PurposeName { get; set; }

        public virtual List<MoneyFlow> MoneyFlows { get; set; }
    }
}
