using ExpensesTracker.Models.Enums;
using CommonStuff;
using System.Data.Entity.Migrations;
using System.Linq;
using ExpensesTracker.Data.Models;

namespace ExpensesTracker.Data.Migrations
{
    internal sealed class FlowConfiguration : DbMigrationsConfiguration<FlowContext>
    {
        public FlowConfiguration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(FlowContext context)
        {
            // setup enums
            var accountTypes = EnumExtensions.EnumToList<AccountTypeEnum>().Select(l => new FlowAccountType() { AccountTypeName = l.ToString(), FlowAccountTypeId = (int)l});
            var porpuses = EnumExtensions.EnumToList<PurposeEnum>().Select(l => new FlowPurpose() { PurposeName = l.ToString(), FlowPurposeId = (int)l });
            var types = EnumExtensions.EnumToList<TypeEnum>().Select(l => new FlowType() { TypeName = l.ToString(), FlowTypeId = (int)l });

            var newAccTypes = accountTypes.Where(l => !context.FlowAccountType.Any(m => l.AccountTypeName.Equals(m.AccountTypeName)));
            foreach (var item in newAccTypes)
            {
                context.FlowAccountType.AddOrUpdate(item);
            }

            var newPorpuses = porpuses.Where(l => !context.FlowPurposes.Any(m => l.PurposeName.Equals(m.PurposeName)));
            foreach (var item in newPorpuses)
            {
                context.FlowPurposes.AddOrUpdate(item);
            }

            var newTypes = types.Where(l => !context.FlowTypes.Any(m => l.TypeName.Equals(m.TypeName)));
            foreach (var item in newTypes)
            {
                context.FlowTypes.AddOrUpdate(item);
            }

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
