namespace ExpensesTracker.Data.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FlowAccountTypes",
                c => new
                    {
                        FlowAccountTypeId = c.Int(nullable: false, identity: true),
                        AccountTypeName = c.String(),
                    })
                .PrimaryKey(t => t.FlowAccountTypeId);
            
            CreateTable(
                "dbo.MoneyFlows",
                c => new
                    {
                        MoneyFlowId = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FlowAccountType_FlowAccountTypeId = c.Int(),
                        FlowPurpose_FlowPurposeId = c.Int(),
                        FlowType_FlowTypeId = c.Int(),
                        FlowUser_FlowUserId = c.Int(),
                    })
                .PrimaryKey(t => t.MoneyFlowId)
                .ForeignKey("dbo.FlowAccountTypes", t => t.FlowAccountType_FlowAccountTypeId)
                .ForeignKey("dbo.FlowPurposes", t => t.FlowPurpose_FlowPurposeId)
                .ForeignKey("dbo.FlowTypes", t => t.FlowType_FlowTypeId)
                .ForeignKey("dbo.FlowUsers", t => t.FlowUser_FlowUserId)
                .Index(t => t.FlowAccountType_FlowAccountTypeId)
                .Index(t => t.FlowPurpose_FlowPurposeId)
                .Index(t => t.FlowType_FlowTypeId)
                .Index(t => t.FlowUser_FlowUserId);
            
            CreateTable(
                "dbo.FlowPurposes",
                c => new
                    {
                        FlowPurposeId = c.Int(nullable: false, identity: true),
                        PurposeName = c.String(),
                    })
                .PrimaryKey(t => t.FlowPurposeId);
            
            CreateTable(
                "dbo.FlowTypes",
                c => new
                    {
                        FlowTypeId = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                    })
                .PrimaryKey(t => t.FlowTypeId);
            
            CreateTable(
                "dbo.FlowUsers",
                c => new
                    {
                        FlowUserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.FlowUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MoneyFlows", "FlowUser_FlowUserId", "dbo.FlowUsers");
            DropForeignKey("dbo.MoneyFlows", "FlowType_FlowTypeId", "dbo.FlowTypes");
            DropForeignKey("dbo.MoneyFlows", "FlowPurpose_FlowPurposeId", "dbo.FlowPurposes");
            DropForeignKey("dbo.MoneyFlows", "FlowAccountType_FlowAccountTypeId", "dbo.FlowAccountTypes");
            DropIndex("dbo.MoneyFlows", new[] { "FlowUser_FlowUserId" });
            DropIndex("dbo.MoneyFlows", new[] { "FlowType_FlowTypeId" });
            DropIndex("dbo.MoneyFlows", new[] { "FlowPurpose_FlowPurposeId" });
            DropIndex("dbo.MoneyFlows", new[] { "FlowAccountType_FlowAccountTypeId" });
            DropTable("dbo.FlowUsers");
            DropTable("dbo.FlowTypes");
            DropTable("dbo.FlowPurposes");
            DropTable("dbo.MoneyFlows");
            DropTable("dbo.FlowAccountTypes");
        }
    }
}
