﻿using System.Data.Entity;
using ExpensesTracker.Data.Models;
using ExpensesTracker.Data.Migrations;

namespace ExpensesTracker.Data
{
    public class FlowContext : DbContext
    {
        public FlowContext() : base("MoneyFlowsDB")
        {
            //Database.SetInitializer(new DropCreateDatabaseAlways<FlowContext>());
            // add config
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<FlowContext, FlowConfiguration>());
        }

        public DbSet<FlowPurpose> FlowPurposes { set; get; }
        public DbSet<FlowAccountType> FlowAccountType { set; get; }
        public DbSet<FlowType> FlowTypes { get; set; }
        public DbSet<FlowUser> FlowUsers { get; set; }
        public DbSet<MoneyFlow> MoneyFlows { get; set; }
    }
}
