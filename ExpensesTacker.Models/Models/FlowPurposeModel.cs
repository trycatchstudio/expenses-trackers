﻿namespace ExpensesTracker.Models
{
    public class FlowPurposeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
