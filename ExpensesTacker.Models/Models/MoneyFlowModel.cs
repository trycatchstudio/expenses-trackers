﻿namespace ExpensesTracker.Models
{
    public class MoneyFlowModel
    {
        public int MoneyFlowId { get; set; }
        public decimal Amount { get; set; }

        public string FlowAccountType { get; set; }
        public string FlowType { get; set; }
        public string FlowPurpose { get; set; }
        public UserModel FlowUser { get; set; }
    }
}
