﻿namespace ExpensesTracker.Models.Enums
{
    public enum TypeEnum
    {
        Unknown = 0,
        Income = 1,
        Outcome = 2
        // something else...
    }
}
