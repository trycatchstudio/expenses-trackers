﻿namespace ExpensesTracker.Models.Enums
{
    public enum PurposeEnum
    {
        Unknown = 0,

        // income
        Salary = 1,

        // outcome
        HouseLoan = 2
    }
}
