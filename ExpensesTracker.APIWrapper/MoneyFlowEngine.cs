﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ExpensesTracker.Models.Standard.Models;
using Newtonsoft.Json;

namespace ExpensesTracker.APIWrapper
{
    public class MoneyFlowEngine
    {
        private string _mediaTypeHeader = "application/json";
        private string _baseURL = "http://expenseslogger.apphb.com/";

        private string UsersURL => $"{_baseURL}api/Users/";
        private string FlowTypesURL => $"{_baseURL}api/FlowTypes/";
        private string FlowPurposesURL => $"{_baseURL}api/FlowPurposes/";
        private string AccountTypesURL => $"{_baseURL}api/AccountTypes/";
        private string MoneyFlowsURL => $"{_baseURL}api/MoneyFlows/";

        #region Users

        public Task<IEnumerable<UserModel>> GetUser()
        {
            return Get<UserModel>(UsersURL);
        }

        public Task<UserModel> GetUser(int id)
        {
            return Get<UserModel>(UsersURL, id);
        }

        public Task<bool> AddUser<UserModel>(UserModel model)
        {
            return Add(UsersURL, model);
        }

        public Task<bool> DeleteUser(int id)
        {
            return Delete(UsersURL, id);
        }

        #endregion

        #region FlowTypes

        public Task<IEnumerable<FlowTypeModel>> GetFlowType()
        {
            return Get<FlowTypeModel>(FlowTypesURL);
        }

        public Task<FlowTypeModel> GetFlowType(int id)
        {
            return Get<FlowTypeModel>(FlowTypesURL, id);
        }

        public Task<bool> AddFlowType(string flowType)
        {
            return Add(FlowTypesURL, flowType);
        }

        public Task<bool> DeleteFlowType(int id)
        {
            return Delete(FlowTypesURL, id);
        }

        #endregion

        #region FlowPurposes

        public Task<IEnumerable<FlowPurposeModel>> GetFlowPurpose()
        {
            return Get<FlowPurposeModel>(FlowPurposesURL);
        }

        public Task<FlowPurposeModel> GetFlowPurpose(int id)
        {
            return Get<FlowPurposeModel>(FlowPurposesURL, id);
        }

        public Task<bool> AddFlowPurpose(string flowPurpose)
        {
            return Add(FlowPurposesURL, flowPurpose);
        }

        public Task<bool> DeleteFlowPurpose(int id)
        {
            return Delete(FlowPurposesURL, id);
        }

        #endregion

        #region AccountTypes

        public Task<IEnumerable<AccountTypeModel>> GetAccountType()
        {
            return Get<AccountTypeModel>(AccountTypesURL);
        }

        public Task<AccountTypeModel> GetAccountType(int id)
        {
            return Get<AccountTypeModel>(AccountTypesURL, id);
        }

        public Task<bool> AddAccountType(string accountType)
        {
            return Add(AccountTypesURL, accountType);
        }

        public Task<bool> DeleteAccountType(int id)
        {
            return Delete(AccountTypesURL, id);
        }

        #endregion

        #region MoneyFlows

        public Task<IEnumerable<MoneyFlowModel>> GetMoneyFlow()
        {
            return Get<MoneyFlowModel>(MoneyFlowsURL);
        }

        public Task<MoneyFlowModel> GetMoneyFlow(int id)
        {
            return Get<MoneyFlowModel>(MoneyFlowsURL, id);
        }

        public Task<bool> AddMoneyFlow<MoneyFlowModel>(MoneyFlowModel model)
        {
            return Add(MoneyFlowsURL, model);
        }

        public Task<bool> DeleteMoneyFlow(int id)
        {
            return Delete(MoneyFlowsURL, id);
        }

        #endregion

        #region privates

        private async Task<IEnumerable<T>> Get<T>(string url)
        {
            IEnumerable<T> obj;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_mediaTypeHeader));
                var response = await client.GetStringAsync(url);

                obj = JsonConvert.DeserializeObject<IEnumerable<T>>(response);
            }

            return obj;
        }

        private async Task<T> Get<T>(string url, int id)
        {
            IEnumerable<T> obj;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_mediaTypeHeader));
                var response = await client.GetStringAsync($"{url}{id}");

                obj = JsonConvert.DeserializeObject<IEnumerable<T>>(response);
            }

            return obj != null ? obj.FirstOrDefault() : default(T);
        }

        private async Task<bool> Add(string url, string s)
        {
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("", s)
                });

                var response = await client.PostAsync($"{url}", content);
                return response.IsSuccessStatusCode;
            }
        }

        private async Task<bool> Add<T>(string url, T t)
        {
            using (var client = new HttpClient())
            {
                var response = await client.PostAsync($"{url}", new StringContent(JsonConvert.SerializeObject(t), Encoding.UTF8, _mediaTypeHeader));
                return response.IsSuccessStatusCode;
            }
        }

        private async Task<bool> Delete(string url, int id)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_mediaTypeHeader));
                var response = await client.DeleteAsync($"{url}{id}");

                return response.IsSuccessStatusCode;
            }
        }

        #endregion
    }
}
