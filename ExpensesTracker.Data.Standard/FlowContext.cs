﻿using ExpensesTracker.Data.Standard.Models;
using Microsoft.EntityFrameworkCore;

namespace ExpensesTracker.Data.Standard
{
    public class FlowContext : DbContext
    {
        public FlowContext() : base()
        {
            //Database.SetInitializer(new DropCreateDatabaseAlways<FlowContext>());
            // add config
            //Database.SetInitializer(new System.Data.Entity.MigrateDatabaseToLatestVersion<FlowContext, FlowConfiguration>());
        }

        public DbSet<FlowPurpose> FlowPurposes { set; get; }
        public DbSet<FlowAccountType> FlowAccountType { set; get; }
        public DbSet<FlowType> FlowTypes { get; set; }
        public DbSet<FlowUser> FlowUsers { get; set; }
        public DbSet<MoneyFlow> MoneyFlows { get; set; }
    }
}
