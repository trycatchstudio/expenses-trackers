﻿namespace ExpensesTracker.Data.Standard.Models
{
    /// <summary>
    /// Actual money flow record. Place where magic happens
    /// </summary>
    public class MoneyFlow
    {
        public int MoneyFlowId { get; set; }
        public decimal Amount { get; set; }

        public FlowAccountType FlowAccountType { get; set; }
        public FlowType FlowType { get; set; }
        public FlowPurpose FlowPurpose { get; set; }
        public FlowUser FlowUser { get; set; }
    }
}
