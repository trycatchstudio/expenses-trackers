﻿using System.Collections.Generic;

namespace ExpensesTracker.Data.Standard.Models
{
    /// <summary>
    /// e.g. salary, expenses for food, bakn credit, etc.
    /// </summary>
    public class FlowPurpose
    {
        public int FlowPurposeId { get; set; }
        public string PurposeName { get; set; }

        public ICollection<MoneyFlow> MoneyFlows { get; set; }
    }
}
