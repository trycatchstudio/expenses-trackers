﻿using System.Collections.Generic;

namespace ExpensesTracker.Data.Standard.Models
{
    public class FlowUser
    {
        public int FlowUserId { get; set; }
        public string UserName { get; set; }

        public ICollection<MoneyFlow> MoneyFlows { get; set; }
    }
}
