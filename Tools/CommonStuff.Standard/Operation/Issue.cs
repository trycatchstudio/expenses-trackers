﻿using System;

namespace CommonStuff.Standard.Operation
{
    public class Issue<TResult>
    {
        public SeverityEnum Severity { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public TResult Result { get; set; }

        public string ResultAsString => Result == null ? null : Result.ToString();

        public int ResultAsInt
        {
            get
            {
                if (Result == null)
                {
                    return 0;
                }

                return Int32.TryParse(Result.ToString(), out int i) ? i : 0;
            }
        }

        public double ResultAsDouble
        {
            get
            {
                if (Result == null)
                {
                    return 0;
                }

                return Double.TryParse(Result.ToString(), out double d) ? d : 0;
            }
        }

        public bool IsSuccess => Severity != SeverityEnum.Error && Severity != SeverityEnum.Exception;

        public static Issue Success => new Issue(SeverityEnum.Information, "Operation completed successfully.");

        public Issue()
        {
            Severity = SeverityEnum.Information;
        }

        public Issue(SeverityEnum severity, string message)
        {
            Severity = severity;
            Message = message;
        }

        public Issue(SeverityEnum severity, string message, params object[] args)
        {
            Severity = severity;
            Message = string.Format(message, args);
        }

        public Issue(Exception ex)
        {
            Severity = SeverityEnum.Exception;
            Message = GetExceptionMessages(ex);
            StackTrace = ex.StackTrace;
        }

        public Issue(TResult result)
        {
            Severity = SeverityEnum.Information;
            Message = "Operation was completed successfully!";
            Result = result;
        }

        public Issue(TResult result, SeverityEnum severity, string message)
        {
            Severity = severity;
            Message = message;
            Result = result;
        }

        public Issue(Issue issueToCopy, bool copyResult = false)
        {
            Severity = issueToCopy.Severity;
            Message = issueToCopy.Message;
            StackTrace = issueToCopy.StackTrace;
            if (copyResult)
            {
                try
                {
                    Result = (TResult) issueToCopy.Result;
                }
                catch (Exception)
                {
                    // Ignore invalid casting of result.
                }
            }
        }

        private static string GetExceptionMessages(Exception e, string message = "")
        {
            if (e == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrWhiteSpace(message))
            {
                message = e.Message;
            }

            if (e.InnerException != null)
            {
                message += string.Format("{0}InnerException: {1}", Environment.NewLine, GetExceptionMessages(e.InnerException));
            }

            return message;
        }
    }

    /// <summary>
    /// TODO: Temporary solution. Should be removed some time in the future.
    /// TODO: Check if object constructor argument is not exception type
    /// Purpose: Issue class was made generic, so to have rest of code compilable this class was added.
    /// </summary>
    public class Issue : Issue<object>
    {
        public Issue()
            : base()
        {
        }

        public Issue(SeverityEnum severity, string message)
            : base(severity, message)
        {
        }

        public Issue(SeverityEnum severity, string message, params object[] args)
            : base(severity, message, args)
        {
        }

        public Issue(Exception ex)
            : base(ex)
        {
        }

        public Issue(object result)
            : base(result)
        {
        }

        public Issue(object result, SeverityEnum severity, string message)
            : base(result, severity, message)
        {
        }
    }
}
