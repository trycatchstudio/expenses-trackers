﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CommonStuff.Core
{
    public class EnumExtensions
    {
        public static IEnumerable<T> EnumToList<T>() where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        } 
    }
}
