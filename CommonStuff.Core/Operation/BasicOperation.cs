﻿namespace CommonStuff.Core.Operation
{
    /// <summary>
    /// Base class for all business logic operations (adds templates to Operation).
    /// </summary>
    /// <typeparam name="TParameter">Operation parameters type</typeparam>
    /// <typeparam name="TResult">Operation result type</typeparam>
    public abstract class BasicOperation<TParameter, TResult> : Operation
        where TResult : class
    {
        /// <summary>
        /// Operation parameters, can be of any type
        /// </summary>
        protected TParameter Parameters { get; set; }

        /// <summary>
        /// Operation result, can be of any type
        /// </summary>
        public TResult OperationResult => Result as TResult;

        public Issue Execute(TParameter parameters)
        {
            Parameters = parameters;
            return ExecuteBase();
        }
    }
}
