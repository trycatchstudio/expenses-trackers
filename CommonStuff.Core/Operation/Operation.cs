﻿using System;

namespace CommonStuff.Core.Operation
{
    /// <summary>
    /// Base class for all business logic operations.
    /// </summary>
    public abstract class Operation
    {
        /// <summary>
        /// Parent operation. Operations can be nested
        /// </summary>
        protected Operation ParentOperation;

        /// <summary>
        /// Result of operation execution
        /// </summary>
        protected object Result;

        /// <summary>
        /// Main function to execute operation
        /// </summary>
        /// <returns>Issue as result</returns>
        protected Issue ExecuteBase()
        {
            try
            {
                // todo: add validate only
                // check rights - todo: need to have user here
                Issue result = CheckAuthorization();
                if (!result.IsSuccess)
                {
                    return result;
                }

                // do actions before executing operation
                result = BeforeExecute();
                if (!result.IsSuccess)
                {
                    return result;
                }

                // execute operation
                result = ExecuteDerived();
                if (!result.IsSuccess)
                {
                    return result;
                }

                Result = result.Result;

                // set success message if empty
                if (string.IsNullOrEmpty(result.Message))
                {
                    result.Message = "Operation completed successfully.";
                }

                // do actions after executing operation
                AfterExecute();

                return result;
            }
            catch (Exception ex)
            {
                return new Issue(ex);
            }
        }

        protected virtual Issue CheckAuthorization()
        {
            return Issue.Success;
        }

        protected virtual Issue BeforeExecute()
        {
            return Issue.Success;
        }

        protected virtual void AfterExecute()
        {
        }

        protected abstract Issue ExecuteDerived();
    }
}
