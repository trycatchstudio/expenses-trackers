﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ExpensesTracker.BusinessLogic.MoneyFlowsOperations;
using ExpensesTracker.Models;

namespace ExpensesTracker.WEB.Controllers
{
    // TODO: setup return format as JSON in global as default
    public class MoneyFlowsController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            var op = new OGetMoneyFlows();
            var res = op.Execute(null);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            var op = new OGetMoneyFlows();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message);
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody]MoneyFlowModel model)
        {
            var op = new OAddMoneyFlow();
            var res = op.Execute(model);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        // PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(int id)
        {
            var op = new ODeleteMoneyFlow();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message);
        }
    }
}