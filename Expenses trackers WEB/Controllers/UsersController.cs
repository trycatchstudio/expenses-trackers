﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ExpensesTracker.BusinessLogic.FlowUserOperations;
using ExpensesTracker.Models;

namespace ExpensesTracker.WEB.Controllers
{
    public class UsersController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            var op = new OGetFlowUsers();
            var res = op.Execute(null);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            var op = new OGetFlowUsers();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] UserModel model)
        {
            var op = new OAddFlowUser();
            var res = op.Execute(model);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        public HttpResponseMessage Delete(int id)
        {
            var op = new ODeleteFlowUser();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }
    }
}