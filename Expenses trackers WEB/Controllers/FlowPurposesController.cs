﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ExpensesTracker.BusinessLogic.FlowPurposesOperations;

namespace ExpensesTracker.WEB.Controllers
{
    public class FlowPurposesController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var op = new OGetFlowPurposes();
            var res = op.Execute(null);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        public HttpResponseMessage Get(int id)
        {
            var op = new OGetFlowPurposes();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        public HttpResponseMessage Post([FromBody] string porpuse)
        {
            var op = new OAddFlowPurpose();
            var res = op.Execute(porpuse);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        public HttpResponseMessage Delete(int id)
        {
            var op = new ODeleteFlowPurposee();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }
    }
}