﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ExpensesTracker.BusinessLogic.FlowAccountTypesOperations;

namespace ExpensesTracker.WEB.Controllers
{
    public class AccountTypesController : ApiController
    {
        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            var op = new OGetAccountTypes();
            var res = op.Execute(null);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        public HttpResponseMessage Get(int id)
        {
            var op = new OGetAccountTypes();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        public HttpResponseMessage Post([FromBody] string type)
        {
            var op = new OAddAccountType();
            var res = op.Execute(type);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }

        public HttpResponseMessage Delete(int id)
        {
            var op = new ODeleteAccountType();
            var res = op.Execute(id);
            if (res.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.OK, res.Result);
            }

            return Request.CreateResponse(HttpStatusCode.InternalServerError, res.Message + Environment.NewLine + res.StackTrace);
        }
    }
}