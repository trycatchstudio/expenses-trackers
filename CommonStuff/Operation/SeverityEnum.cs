﻿namespace CommonStuff.Operation
{
    public enum SeverityEnum
    {
        Verbose = 0,
        Start = 1,
        Stop = 2,
        Information = 3,
        Warning = 4,
        Error = 5,
        Exception = 6
    }
}
