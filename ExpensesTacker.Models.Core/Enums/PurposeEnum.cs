﻿namespace ExpensesTracker.Models.Enums.Core
{
    public enum PurposeEnum
    {
        Unknown = 0,

        // income
        Salary = 1,

        // outcome
        HouseLoan = 2
    }
}
