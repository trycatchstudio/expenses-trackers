﻿namespace ExpensesTracker.Models.Enums.Core
{
    public enum TypeEnum
    {
        Unknown = 0,
        Income = 1,
        Outcome = 2
        // something else...
    }
}
