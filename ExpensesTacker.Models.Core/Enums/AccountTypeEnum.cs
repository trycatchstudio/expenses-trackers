﻿namespace ExpensesTracker.Models.Enums.Core
{
    public enum AccountTypeEnum
    {
        Unknown = 0,
        Cash = 1,
        BankAccount = 2,
        Credit = 3
    }
}
