﻿namespace ExpensesTracker.Models.Core
{
    public class FlowPurposeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
