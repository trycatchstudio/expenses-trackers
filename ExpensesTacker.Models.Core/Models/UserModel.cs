﻿namespace ExpensesTracker.Models.Core
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public UserModel(int id)
        {
            Id = id;
        }

        public UserModel()
        {
        }
    }
}
