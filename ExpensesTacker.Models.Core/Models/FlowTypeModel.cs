﻿namespace ExpensesTracker.Models.Core
{
    public class FlowTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
