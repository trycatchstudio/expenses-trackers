﻿namespace ExpensesTracker.Models.Core
{
    public class AccountTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
