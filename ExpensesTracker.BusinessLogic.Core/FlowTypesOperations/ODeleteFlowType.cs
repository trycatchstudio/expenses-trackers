﻿using System.Linq;
using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;

namespace ExpensesTracker.BusinessLogic.Core.FlowTypesOperations
{
    public class ODeleteFlowType : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.FlowTypes.FirstOrDefault(l => l.FlowTypeId == Parameters);
            if (item != null)
            {
                _context.FlowTypes.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"Type with id '{Parameters}' not found!");
        }
    }
}
