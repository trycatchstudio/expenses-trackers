﻿using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;
using ExpensesTracker.Data.Core.Models;

namespace ExpensesTracker.BusinessLogic.Core.FlowTypesOperations
{
    public class OAddFlowType : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New Flow type not provided");
            }

            var ft = new FlowType();

            ft.TypeName = Parameters;

            // TODO: check if unique
            _context.FlowTypes.Add(ft);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
