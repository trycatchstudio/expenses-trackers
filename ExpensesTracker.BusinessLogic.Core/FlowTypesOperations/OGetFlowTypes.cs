﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;
using ExpensesTracker.BusinessLogic.Core.Factories;
using ExpensesTracker.Models.Core;

namespace ExpensesTracker.BusinessLogic.Core.FlowTypesOperations
{
    public class OGetFlowTypes : BaseMoneyFlowOperation<int?, IEnumerable<FlowTypeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowTypes.ToList().Where(l => l.FlowTypeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowTypes.ToList().Select(ModelFactory.Create));
        }
    }
}
