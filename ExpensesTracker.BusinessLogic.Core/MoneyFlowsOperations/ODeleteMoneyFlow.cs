﻿using System.Linq;
using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;

namespace ExpensesTracker.BusinessLogic.Core.MoneyFlowsOperations
{
    public class ODeleteMoneyFlow : BaseMoneyFlowOperation<int, object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.MoneyFlows.FirstOrDefault(l => l.MoneyFlowId == Parameters);
            if (item != null)
            {
                _context.MoneyFlows.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"Money flow with id '{Parameters}' not found!");
        }
    }
}
