﻿using System.Linq;
using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;
using ExpensesTracker.BusinessLogic.Core.Factories;
using ExpensesTracker.Models.Core;

namespace ExpensesTracker.BusinessLogic.Core.FlowUserOperations
{
    public class OGetFlowUsers : BaseMoneyFlowOperation<int?, UserModel>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowUsers.ToList().Where(l => l.FlowUserId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowUsers.ToList().Select(ModelFactory.Create));
        }
    }
}
