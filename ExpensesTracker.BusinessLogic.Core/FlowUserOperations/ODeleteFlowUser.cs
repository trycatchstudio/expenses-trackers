﻿using System.Linq;
using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;

namespace ExpensesTracker.BusinessLogic.Core.FlowUserOperations
{
    public class ODeleteFlowUser : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.FlowUsers.FirstOrDefault(l => l.FlowUserId == Parameters);
            if (item != null)
            {
                _context.FlowUsers.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"User with id '{Parameters}' not found!");
        }
    }
}
