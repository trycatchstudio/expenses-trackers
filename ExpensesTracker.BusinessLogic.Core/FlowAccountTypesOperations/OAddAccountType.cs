﻿using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;
using ExpensesTracker.Data.Core.Models;

namespace ExpensesTracker.BusinessLogic.Core.FlowAccountTypesOperations
{
    public class OAddAccountType : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New account type not provided");
            }

            var ft = new FlowAccountType();

            ft.AccountTypeName = Parameters;

            // TODO: check if unique
            _context.FlowAccountType.Add(ft);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
