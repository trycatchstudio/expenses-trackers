﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;
using ExpensesTracker.BusinessLogic.Core.Factories;
using ExpensesTracker.Models.Core;

namespace ExpensesTracker.BusinessLogic.Core.FlowAccountTypesOperations
{
    public class OGetAccountTypes : BaseMoneyFlowOperation<int?, IEnumerable<AccountTypeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowAccountType.ToList().Where(l => l.FlowAccountTypeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowAccountType.ToList().Select(ModelFactory.Create));
        }
    }
}
