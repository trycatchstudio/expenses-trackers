﻿using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;
using ExpensesTracker.Data.Core.Models;

namespace ExpensesTracker.BusinessLogic.Core.FlowPurposesOperations
{
    public class OAddFlowPurpose : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New flow purpose not provided");
            }

            var fp = new FlowPurpose();

            fp.PurposeName = Parameters;

            // check if unique
            _context.FlowPurposes.Add(fp);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
