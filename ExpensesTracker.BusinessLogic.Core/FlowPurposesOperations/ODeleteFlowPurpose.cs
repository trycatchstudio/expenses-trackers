﻿using System.Linq;
using CommonStuff.Core.Operation;
using ExpensesTracker.BusinessLogic.Core.BaseOperation;

namespace ExpensesTracker.BusinessLogic.Core.FlowPurposesOperations
{
    public class ODeleteFlowPurposee : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.FlowPurposes.FirstOrDefault(l => l.FlowPurposeId == Parameters);
            if (item != null)
            {
                _context.FlowPurposes.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"Purpose with id '{Parameters}' not found!");
        }
    }
}
