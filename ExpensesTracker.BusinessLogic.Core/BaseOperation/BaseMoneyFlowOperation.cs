﻿using System;
using CommonStuff.Core.Operation;
using ExpensesTracker.Data.Core;
using Microsoft.EntityFrameworkCore;

namespace ExpensesTracker.BusinessLogic.Core.BaseOperation
{
    public abstract class BaseMoneyFlowOperation<TParameter, TResult> : BasicOperation<TParameter, TResult> where TResult : class
    {
        //https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/crud
        //https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro

        internal FlowContext _context;

        protected override Issue BeforeExecute()
        {
            try
            {
                // get config value
                var optionsBuilder = new DbContextOptionsBuilder<FlowContext>();

                optionsBuilder.UseSqlServer("Data Source=(LocalDb)\v11.0; Initial Catalog = FlowDB; Integrated Security = True; MultipleActiveResultSets = True;");
                _context = new FlowContext(optionsBuilder.Options);

                return Issue.Success;
            }
            catch (Exception ex)
            {
                return new Issue(ex);
            }
        }

        protected override void AfterExecute()
        {
            try
            {
                // should dispose _context here. Maybe there is some way to use Using block in base class
                _context.Dispose();
            }
            catch (Exception ex)
            {
                // error or shit...
            }
        }
    }
}
