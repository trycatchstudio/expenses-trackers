﻿using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.Data.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowPurposesOperations
{
    public class OAddFlowPurpose : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New flow purpose not provided");
            }

            var fp = new FlowPurpose();

            fp.PurposeName = Parameters;

            // check if unique
            _context.FlowPurposes.Add(fp);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
