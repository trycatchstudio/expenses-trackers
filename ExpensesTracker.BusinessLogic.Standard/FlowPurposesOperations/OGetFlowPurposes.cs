﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.BusinessLogic.Standard.Factories;
using ExpensesTracker.Models.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowPurposesOperations
{
    public class OGetFlowPurposes : BaseMoneyFlowOperation<int?, IEnumerable<FlowPurposeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowPurposes.ToList().Where(l => l.FlowPurposeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowPurposes.ToList().Select(ModelFactory.Create));
        }
    }
}
