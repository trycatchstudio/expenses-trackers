﻿using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using System.Linq;

namespace ExpensesTracker.BusinessLogic.Standard.FlowPurposesOperations
{
    public class ODeleteFlowPurposee : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.FlowPurposes.FirstOrDefault(l => l.FlowPurposeId == Parameters);
            if (item != null)
            {
                _context.FlowPurposes.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"Purpose with id '{Parameters}' not found!");
        }
    }
}
