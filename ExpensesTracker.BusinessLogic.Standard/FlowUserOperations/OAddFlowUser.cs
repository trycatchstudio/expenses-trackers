﻿using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.BusinessLogic.Standard.Factories;
using ExpensesTracker.Models.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowUserOperations
{
    public class OAddFlowUser : BaseMoneyFlowOperation<UserModel, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters == null)
            {
                return new Issue(SeverityEnum.Error, "Model is not provided");
            }

            var user = DataFactory.Create(Parameters);

            if (user == null)
            {
                return new Issue(SeverityEnum.Error, "Unable to create DB record from model");
            }

            // validate, that all properties referencing other tables are there
            if (string.IsNullOrWhiteSpace(user.UserName))
            {
                return new Issue(SeverityEnum.Error, "User name not provided");
            }

            if (_context.FlowUsers.ToList().Any(l => l.UserName == user.UserName))
            {
                return new Issue(SeverityEnum.Error, "User name already exist");
            }

            _context.FlowUsers.Add(user);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
