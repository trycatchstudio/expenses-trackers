﻿using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.BusinessLogic.Standard.Factories;
using ExpensesTracker.Models.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowUserOperations
{
    public class OGetFlowUsers : BaseMoneyFlowOperation<int?, UserModel>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowUsers.ToList().Where(l => l.FlowUserId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowUsers.ToList().Select(ModelFactory.Create));
        }
    }
}
