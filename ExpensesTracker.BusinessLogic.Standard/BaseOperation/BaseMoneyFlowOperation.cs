﻿using System;
using CommonStuff.Standard.Operation;
using ExpensesTracker.Data.Standard;

namespace ExpensesTracker.BusinessLogic.Standard.BaseOperation
{
    public abstract class BaseMoneyFlowOperation<TParameter, TResult> : BasicOperation<TParameter, TResult> where TResult : class
    {
        //https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/crud
        //https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro

        internal FlowContext _context;

        protected override Issue BeforeExecute()
        {
            try
            {
                _context = new FlowContext();
                return Issue.Success;
            }
            catch (Exception ex)
            {
                return new Issue(ex);
            }
        }

        protected override void AfterExecute()
        {
            try
            {
                // should dispose _context here. Maybe there is some way to use Using block in base class
                _context.Dispose();
            }
            catch (Exception ex)
            {
                // error or shit...
            }
        }
    }
}
