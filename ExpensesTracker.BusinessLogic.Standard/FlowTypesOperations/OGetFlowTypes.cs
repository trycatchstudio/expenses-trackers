﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.BusinessLogic.Standard.Factories;
using ExpensesTracker.Models.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowTypesOperations
{
    public class OGetFlowTypes : BaseMoneyFlowOperation<int?, IEnumerable<FlowTypeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowTypes.ToList().Where(l => l.FlowTypeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowTypes.ToList().Select(ModelFactory.Create));
        }
    }
}
