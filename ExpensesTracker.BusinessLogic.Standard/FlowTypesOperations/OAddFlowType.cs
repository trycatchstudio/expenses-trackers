﻿using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.Data.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowTypesOperations
{
    public class OAddFlowType : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New Flow type not provided");
            }

            var ft = new FlowType();

            ft.TypeName = Parameters;

            // TODO: check if unique
            _context.FlowTypes.Add(ft);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
