﻿using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;

namespace ExpensesTracker.BusinessLogic.Standard.FlowTypesOperations
{
    public class ODeleteFlowType : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.FlowTypes.FirstOrDefault(l => l.FlowTypeId == Parameters);
            if (item != null)
            {
                _context.FlowTypes.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"Type with id '{Parameters}' not found!");
        }
    }
}
