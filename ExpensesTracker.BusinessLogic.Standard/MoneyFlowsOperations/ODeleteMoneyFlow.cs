﻿using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using System.Linq;

namespace ExpensesTracker.BusinessLogic.Standard.MoneyFlowsOperations
{
    public class ODeleteMoneyFlow : BaseMoneyFlowOperation<int, object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.MoneyFlows.FirstOrDefault(l => l.MoneyFlowId == Parameters);
            if (item != null)
            {
                _context.MoneyFlows.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"Money flow with id '{Parameters}' not found!");
        }
    }
}
