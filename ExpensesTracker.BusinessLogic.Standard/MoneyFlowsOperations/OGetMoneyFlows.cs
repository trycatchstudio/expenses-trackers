﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.BusinessLogic.Standard.Factories;
using ExpensesTracker.Models.Standard.Models;
using Microsoft.EntityFrameworkCore;

namespace ExpensesTracker.BusinessLogic.Standard.MoneyFlowsOperations
{
    public class OGetMoneyFlows : BaseMoneyFlowOperation<int?, IEnumerable<MoneyFlowModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.MoneyFlows
                                         .Where(l => l.MoneyFlowId == Parameters.Value)
                                         .Include(c => c.FlowAccountType)
                                         .Include(c => c.FlowPurpose)
                                         .Include(c => c.FlowType)
                                         .Include(c => c.FlowUser)
                                         .ToList().Select(ModelFactory.Create));
            }

            return new Issue(_context.MoneyFlows
                                     .Include(c => c.FlowAccountType)
                                     .Include(c => c.FlowPurpose)
                                     .Include(c => c.FlowType)
                                     .Include(c => c.FlowUser)
                                     .ToList().Select(ModelFactory.Create));
        }
    }
}
