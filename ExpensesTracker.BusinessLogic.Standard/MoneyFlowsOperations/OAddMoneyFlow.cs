﻿using System;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.BusinessLogic.Standard.Factories;
using ExpensesTracker.Models.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.MoneyFlowsOperations
{
    public class OAddMoneyFlow : BaseMoneyFlowOperation<MoneyFlowModel,object>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters == null)
            {
                return new Issue(SeverityEnum.Error, "Model is not provided");
            }

            var mf = DataFactory.Create(_context, Parameters);

            if (mf == null)
            {
                return new Issue(SeverityEnum.Error, "Unable to create DB record from model");
            }

            // validate, that all properties referencing other tables are there
            string errors = String.Empty;

            if (mf.FlowAccountType == null)
            {
                errors += "Account type does not exist" + Environment.NewLine;
            }

            if (mf.FlowPurpose == null)
            {
                errors += "Purpose does not exist" + Environment.NewLine;
            }

            if (mf.FlowType == null)
            {
                errors += "Type does not exist" + Environment.NewLine;
            }

            if (mf.FlowUser == null)
            {
                errors += "User does not exist" + Environment.NewLine;
            }

            if (!string.IsNullOrWhiteSpace(errors))
            {
                return new Issue(SeverityEnum.Error, errors.Trim());
            }

            _context.MoneyFlows.Add(mf);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
