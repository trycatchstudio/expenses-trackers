﻿using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.Data.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowAccountTypesOperations
{
    public class OAddAccountType : BaseMoneyFlowOperation<string, object>
    {
        protected override Issue ExecuteDerived()
        {
            if (string.IsNullOrWhiteSpace(Parameters))
            {
                return new Issue(SeverityEnum.Error, "New account type not provided");
            }

            var ft = new FlowAccountType();

            ft.AccountTypeName = Parameters;

            // TODO: check if unique
            _context.FlowAccountType.Add(ft);
            _context.SaveChanges();

            return Issue.Success;
        }
    }
}
