﻿using System.Collections.Generic;
using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;
using ExpensesTracker.BusinessLogic.Standard.Factories;
using ExpensesTracker.Models.Standard.Models;

namespace ExpensesTracker.BusinessLogic.Standard.FlowAccountTypesOperations
{
    public class OGetAccountTypes : BaseMoneyFlowOperation<int?, IEnumerable<AccountTypeModel>>
    {
        protected override Issue ExecuteDerived()
        {
            if (Parameters.HasValue)
            {
                return new Issue(_context.FlowAccountType.ToList().Where(l => l.FlowAccountTypeId == Parameters.Value).Select(ModelFactory.Create));
            }

            return new Issue(_context.FlowAccountType.ToList().Select(ModelFactory.Create));
        }
    }
}
