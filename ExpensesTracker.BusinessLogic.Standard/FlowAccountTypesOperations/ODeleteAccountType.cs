﻿using System.Linq;
using CommonStuff.Standard.Operation;
using ExpensesTracker.BusinessLogic.Standard.BaseOperation;

namespace ExpensesTracker.BusinessLogic.Standard.FlowAccountTypesOperations
{
    public class ODeleteAccountType : BaseMoneyFlowOperation<int,object>
    {
        protected override Issue ExecuteDerived()
        {
            var item = _context.FlowAccountType.FirstOrDefault(l => l.FlowAccountTypeId == Parameters);
            if (item != null)
            {
                _context.FlowAccountType.Remove(item);
                _context.SaveChanges();
                return Issue.Success;
            }

            return new Issue(SeverityEnum.Error, $"Account type with id '{Parameters}' not found!" );
        }
    }
}
